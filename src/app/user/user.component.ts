import { Component } from '@angular/core';
import { GamesComponent } from '../games/games.component';
import {
  FormControl,
  FormsModule,
  ReactiveFormsModule,
  FormGroup,
} from '@angular/forms';

@Component({
  selector: 'app-user',
  standalone: true,
  imports: [GamesComponent, FormsModule, ReactiveFormsModule],
  templateUrl: './user.component.html',
  styleUrl: './user.component.css',
})
export class UserComponent {
  profileForm = new FormGroup({
    name: new FormControl(''),
    email: new FormControl(''),
  });

  username = 'alejandro';
  isLoggedIn = false; // <- está funcionando como un state (estado)
  favGame = '';
  favoriteFramework = '';

  getFavorite(gameName: string) {
    this.favGame = gameName;
  }

  greet() {
    alert(`Hello ${this.username}!`);
  }

  showFramework() {
    alert(this.favoriteFramework);
  }

  handleSubmit() {
    alert(this.profileForm.value.name + ' | ' + this.profileForm.value.email);
  }
}
