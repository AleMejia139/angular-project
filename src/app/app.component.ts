import { Component, inject } from '@angular/core';
import { RouterLink, RouterOutlet } from '@angular/router';
import { CarserviceService } from './carservice.service';
import { CurrencyPipe, DatePipe, DecimalPipe, LowerCasePipe, UpperCasePipe } from '@angular/common';
import { ReversePipe } from './pipes/reverse.pipe';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, RouterLink, UpperCasePipe, LowerCasePipe, DecimalPipe, DatePipe, CurrencyPipe, ReversePipe],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
})
export class AppComponent {
  title = 'angular-17-app';

  darkMode() {
    document.body.classList.toggle('bg-dark');
    document.body.classList.toggle('text-white');
  }

  //carService = inject(CarserviceService);
  display = '';

  constructor(private carService: CarserviceService) {
    this.display = this.carService.getCars().join('⭐️');
  }

  username = 'alEjaNdrO';

  loudMessage = 'we think you are doing great';

  num = 103.1234;
  birthday = new Date(2023, 3, 2);
  cost = 4560.34;

  word = 'You are a champion';
}

