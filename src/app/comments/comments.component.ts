import { NgOptimizedImage } from '@angular/common';
import { Component } from '@angular/core';

@Component({
  selector: 'app-comments',
  standalone: true,
  imports: [NgOptimizedImage],
  template: `
    <h3>Comentarios de lo que sea</h3>
    <picture>
      <img
        ngSrc="https://img2.rtve.es/i/?w=1600&i=1614352806474.png"
        alt="Image"
        class="img-thumbnail"
        priority
        height="900"
        width="1600"
      />
    </picture>
    <p>
      Lorem ipsum, dolor sit amet consectetur adipisicing elit. Delectus nemo ad
      sapiente repellat blanditiis possimus aspernatur. Asperiores, quam
      corporis facilis cupiditate, veritatis atque impedit dolore, recusandae
      maxime aliquid nihil modi.
    </p>
  `,
  styles: ``,
})
export class CommentsComponent {}
